package md.sorbalo.jms.listener;

import static org.junit.Assert.*;

import java.net.UnknownHostException;

import md.sorbalo.jms.adapter.ConsumerAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConsumerAdapterTest {
	
	private String json = "{vendorName:\"Microsofttest\",firstName:\"BobTest\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongo() throws UnknownHostException {
		ConsumerAdapter adapter = new ConsumerAdapter();
		adapter.sendToMongo(json);
		assertNotNull(json);
	}

}
