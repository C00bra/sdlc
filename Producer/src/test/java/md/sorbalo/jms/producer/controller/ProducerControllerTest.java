package md.sorbalo.jms.producer.controller;

import static org.junit.Assert.*;
import md.sorbalo.jms.producer.model.Vendor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

public class ProducerControllerTest {
	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext context;

	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController = (ProducerController)context.getBean("producerController");
		vendor = new Vendor();
		vendor.setVendorName("TNT");
		vendor.setFirstName("Bob");
		vendor.setLastName("Thomas");
		vendor.setAddress("123 Main st.");
		vendor.setCity("Maintown");
		vendor.setState("Iowa");
		vendor.setZipcode("1234");
		vendor.setEmail("bob@tnt.com");
		vendor.setPhoneNumber("123-456-7890");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index", producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());
	}

}
